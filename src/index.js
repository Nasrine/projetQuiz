
//Section Questions
let questionNo = document.querySelector("#questionNo");
let questionText = document.querySelector("#questionText");
let correct = 0;

//Plusieurs Réponses
let option1 = document.querySelector("#option1");
let option2 = document.querySelector("#option2");
let option3 = document.querySelector("#option3");
let option4 = document.querySelector("#option4");

//Tous les 'H4' du  Quiz (MQCM)
let choice_que = document.querySelectorAll(".choice_que");


let suivant= document.querySelector ("#start");
//je declare mes object


let MQCM = [{
    question: "Quand Blanche-Neige s'enfuit de chez elle, de quoi a-t-elle peur  ?",
    choice1: "De la lune",
    choice2: "De la pluie",
    choice3: "Des Arbres",
    choice4: "Des Loups",
    answer: 2
},
            {
            question: "Comment s’appelle le jeune cerf ennemi de Bambi ??",
    choice1: "Donno",
    choice2: "Konno",
    choice3: " Ronno",
    choice4: "Fonno",
    answer: 2
},
            {
            question: "Quel personnage meurt dans le film Pocahontas ?",
    choice1: "John Smith",
    choice2: "Kocoum",
    choice3: "Meeko",
    choice4: "John Ratcliffe",
    answer: 1
},
            {
            question: "Combien la Petite Sirène a-t-elle de soeurs ?",
    choice1: "4",
    choice2: "6",
    choice3: "8",
    choice4: "1",
    answer: 1
},
            {
            question: "Que répète sans cesse le Lapin Blanc dans alice au pays des merveilles?",
    choice1: "J'suis en r'tard",
    choice2: "Vite, vite !",
    choice3: "  J'suis pressé",
    choice4: "j'ai faim!",
    answer: 0
},
            {
            question: "Combien y a-t-il de chiens à la fin du film du 101 dalmatien ??",
    choice1: "Comme au début",
    choice2: "75",
    choice3: " 90",
    choice4: "101",
    answer: 3
},
            {
               question: "Comment s'appelle leur chef cruel dans Mulan??",
    choice1: "Shan-Yu",
    choice2: "Attila",
    choice3: "Fu Manchu",
    choice4: "Majda",
    answer: 0
},{
               question: "Qui est Scar ??",
    choice1: "Le frère de Mufasa",
    choice2: "le père de Simba",
    choice3: "Le frère de Simba",
    choice4: "La mère de Simba",
    answer: 0
},
            {
               question: "Comment se nomme le pays où vit Peter Pan ?",
    choice1: "Le Pays Invisible",
    choice2: "  Le Pays Irréel",
    choice3: "  Le Pays Imaginaire",
    choice4: "A Grenoble",
    answer: 2
},
            {
               question: "Quelle est la phrase favorite de Timon et Pumbaa ?",
    choice1: "Pas de souci",
    choice2: "Hopla Matata",
    choice3: "Maramé ooohw",
    choice4: "Hakuna Matata",
    answer: 3
}];


let index = 0;



let loadData = () => {
    questionNo.textContent= index + 1 + ". ";
    questionText.textContent = MQCM[index].question;
    option1.textContent = MQCM[index].choice1;
    option2.textContent = MQCM[index].choice2;
    option3.textContent = MQCM[index].choice3;
    option4.textContent= MQCM[index].choice4;
    


}

    suivant.click();

//bouton Suivant
suivant.addEventListener('click', () => {

    if (index !== MQCM.length - 1) {
        index++;
         removeActiveClass(choice_que)
        clearInterval(interval);

        // Mes questions
        loadData();
        total_correct.textContent = "vous avez "+ correct + " points sur " + MQCM.length + " questions";


    }
    

    for (i = 0; i <= 3; i++) {
        choice_que[i].classList.remove("choixred", "choixvert");
        choice_que[i].classList.remove("disabled")



    }
})


loadData();

// reponse et Calcul des points

choice_que.forEach((choices, choiceNo) => {
    choices.addEventListener("click", () => {
        choices.classList.add("active");
        //check la bonne réponse
        if (choiceNo === MQCM[index].answer) {
            correct++;
            choices.classList.add('choixvert')
        } else {
            correct += 0;
            choices.classList.add('choixred');
        }
        //stop Compteur
        clearInterval(interval);

        //Descativez toutes les classes
        for (i = 0; i <= 3; i++) {
            choice_que[i].classList.add("disabled");
        }
    })
});
let interval = 0
//verification reponse et Calcul des points


function removeActiveClass(paramClass) {
    paramClass.forEach(removeActive => {
        removeActive.classList.remove("active");
    })
}

//Variable pour capturer le total 
let toto = document.querySelector("#total_correct");
let next_question = document.querySelector("#next_question");

toto.textContent=('10 points')